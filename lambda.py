"""lambda"""
list1 = [1, 2, 3]
list2 = [10, 9, 8]
output=map(lambda x, y: x + y, list2, list2)
print (list(output))
pairs = [(1, 'one'), (2, 'two'), (3, 'three'), (4, 'four')]
pairs.sort(key=lambda pair: pair[1])
print (pairs)

#filter
a = [1, 2, 3, 5, 6,20,-1,10]
output1=filter(lambda x : x >3 , a) 
print (list(output1))

dict=[{'name':'joe','rank':'5'},{'name':'viki','rank':'15'},{'name':'joy','rank':'7'}]
print (list(filter(lambda x:x['name']=='joy',dict)))