#1
def wish():
	print ("Good Morning")
wish_them=wish
print(wish_them())

#2
def say_hello(say_hi_func):
  print("Hello")
  
  say_hi_func()

def say_hi():
  print("Hi")
  
say_hello(say_hi)

#3
def parent():
  print("I am parent function")
  
  def child():
    print("I am child function") 
  child()  
parent() 
#child()-here child is out of scope

"""zip"""
list_a = [1, 2, 3, 4, 5]
list_b = ['a', 'b', 'c', 'd', 'e']

zip_list = zip(list_a, list_b)

print(list(zip_list))