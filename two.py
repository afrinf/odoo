"""lists"""
list1 = ['red', 'black', 19, 21];
list2 = [7,2,3,10,1,5,4];
print "list1[0]: ", list1[0]
print "list2[0:5]: ", list2[0:5]
#updating list
list1[2]="green"
print list1
del list1[2]
print list1
print cmp(list1,list2)
#converts tuple to list
tup=(1,'good',3,'morning')
newl=list(tup)
list1.extend(list2)
print list1
list2.sort()
print list2

"""tuple"""
tup1=('one','two','three','four','five')
tup2=('ab','cd','ef','gh','ij')
print tup1[0]
print tup2[2:]